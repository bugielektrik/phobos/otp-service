// nolint: funlen
package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/hashicorp/go-hclog"
	"github.com/twilio/twilio-go"

	"gitlab.com/bugielektrik/phobos/otp-service/internal/api"
	"gitlab.com/bugielektrik/phobos/otp-service/internal/config"
	"gitlab.com/bugielektrik/phobos/otp-service/internal/service"
	"gitlab.com/bugielektrik/phobos/otp-service/internal/store/postgres"
	"gitlab.com/bugielektrik/phobos/otp-service/pkg/database"
	"gitlab.com/bugielektrik/phobos/otp-service/pkg/logger"
)

const (
	configsDir = "configs"
	project    = "notification"
)

// Run initializes whole application.
func main() {
	hcLogger := hclog.New(&hclog.LoggerOptions{
		JSONFormat: false,
		Level:      hclog.Debug,
	})

	cfg, err := config.Init(configsDir, project)
	if err != nil {
		logger.Error(err)
		return
	}

	// Dependencies
	postgresDB, err := database.New(cfg.Postgres.DataSourceName, project)
	if err != nil {
		logger.Error(err)
		return
	}

	err = database.Migrate(cfg.Postgres.DataSourceName)
	if err != nil {
		logger.Error(err)
		return
	}

	client := twilio.NewRestClientWithParams(twilio.ClientParams{
		Username:   cfg.Twilio.Username,
		Password:   cfg.Twilio.Password,
		AccountSid: cfg.Twilio.ServiceSid,
	})

	// Services, Repos & API Handlers
	stores := postgres.New(postgresDB, hcLogger)

	services := service.New(service.Dependencies{
		Client:      client,
		OtpAttempts: cfg.Otp.Attempts,
		OtpInterval: cfg.Otp.Interval,
		OtpStore:    stores.Otp,
	})

	handlers := api.New(api.Dependencies{
		OtpService: services.Otp,
	})

	// Cron
	handlers.InitCron()

	// HTTP Server
	server := handlers.InitRest(cfg)

	// Listen from a different goroutine
	go func() {
		if err := server.Listen(":" + cfg.HTTP.Port); err != nil {
			log.Panic(err)
		}
	}()

	logger.Info("Server started")

	// Graceful Shutdown
	quit := make(chan os.Signal, 1)                    // Create channel to signify a signal being sent
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM) // When an interrupt or termination signal is sent, notify the channel

	_ = <-quit // This blocks the main thread until an interrupt is received
	fmt.Println("Gracefully shutting down...")
	_ = server.Shutdown()

	fmt.Println("Running cleanup tasks...")
	// Your cleanup tasks go here
	if err := postgresDB.Close(); err != nil {
		logger.Error(err.Error())
	}

	fmt.Println("Fiber was successful shutdown.")
}
