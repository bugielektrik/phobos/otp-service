package service

import (
	"context"
	"errors"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/twilio/twilio-go"
	openapi "github.com/twilio/twilio-go/rest/api/v2010"
	"github.com/xlzd/gotp"

	"gitlab.com/bugielektrik/phobos/otp-service/internal/model"
)

type OtpStore interface {
	Create(data *model.Otp) error
	GetByKey(key string) (*model.Otp, error)
	DeleteExpiredTokens(otpInterval string)
	Lock(ctx context.Context, key string) (func(), *sqlx.Tx, error)
	Update(ctx context.Context, tx *sqlx.Tx, key string, data *model.OtpInstance) error
}

type Otp struct {
	client   *twilio.RestClient
	attempts int
	interval int
	store    OtpStore
}

func NewOtpService(client *twilio.RestClient, attempts, interval int, store OtpStore) *Otp {
	return &Otp{
		client:   client,
		attempts: attempts,
		interval: interval,
		store:    store,
	}
}

func (s *Otp) Create(phone, debug string) (*model.Otp, error) {
	// Get new secret four-digit code
	otpKey := uuid.New().String()
	otpSecret := gotp.RandomSecret(16)
	otpCode := gotp.NewTOTP(otpSecret, 4, s.interval, nil).Now()

	// Generate additional otp data and save to DB
	otpDest := &model.Otp{
		Key:    otpKey,
		Secret: otpSecret,
		Phone:  phone,
		Status: "1",
		SendAt: time.Now().Unix(),
	}

	err := s.store.Create(otpDest)
	if err != nil {
		return nil, err
	}

	if debug == "true" {
		otpDest.OTP = otpCode
	} else {
		params := &openapi.CreateMessageParams{}
		params.SetTo(phone)
		params.SetBody("Никому не говорите код! Код: " + otpCode)

		_, err := s.client.ApiV2010.CreateMessage(params)
		if err != nil {
			return nil, err
		}
	}

	return otpDest, nil
}

func (s *Otp) GetByID(ctx context.Context, otpDest *model.Otp) (*model.Otp, error) {
	requestTime := time.Now().Unix()

	otpSrc, err := s.store.GetByKey(otpDest.Key)
	if err != nil {
		return nil, err
	}

	if otpSrc == nil {
		return nil, nil
	}

	otpIns := &model.OtpInstance{}

	// check if otp is not active
	if otpSrc.Status != "1" {
		err = errors.New("otp is no longer valid")
	}

	// check if otp has expired
	if err == nil && requestTime-otpSrc.SendAt > int64(s.interval) {
		otpIns.Status = &[]string{"0"}[0]
		err = errors.New("otp is expired")
	}

	otpSrc.Attempts += 1
	if err == nil && otpSrc.Attempts > s.attempts {
		otpIns.Status = &[]string{"0"}[0]
		err = errors.New("you reached maximum attempts")
	}

	if err == nil {
		// validate otp
		valid := gotp.NewTOTP(otpSrc.Secret, 4, s.interval, nil).Verify(otpDest.OTP, int(otpSrc.SendAt))
		if !valid {
			err = errors.New("code is invalid")
		} else {
			otpIns.Status = &[]string{"0"}[0]
			otpIns.ConfirmedAt = &[]int64{time.Now().Unix()}[0]
		}
	}

	if otpSrc.Attempts <= (s.attempts + 1) {
		otpIns.Attempts = &[]int{otpSrc.Attempts}[0]

		// lock otp
		unlockOTP, reqTx, err := s.store.Lock(ctx, otpSrc.Key)
		if err != nil {
			return nil, err
		}
		defer unlockOTP()

		// set otp status and details
		err = s.store.Update(ctx, reqTx, otpSrc.Key, otpIns)
	}

	return otpSrc, err
}

func (s *Otp) DeleteExpiredTokens() {
	s.store.DeleteExpiredTokens(strconv.Itoa(s.interval))
}
