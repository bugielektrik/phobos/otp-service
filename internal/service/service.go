package service

import "github.com/twilio/twilio-go"

type Dependencies struct {
	Client      *twilio.RestClient
	OtpAttempts int
	OtpInterval int
	OtpStore    OtpStore
}

type Service struct {
	Otp *Otp
}

func New(d Dependencies) *Service {
	return &Service{
		Otp: NewOtpService(d.Client, d.OtpAttempts, d.OtpInterval, d.OtpStore),
	}
}
