package model

type Otp struct {
	ID          string `json:"-" db:"id"`
	Key         string `json:"key" db:"key" validate:"required"`
	Secret      string `json:"-" db:"secret"`
	Phone       string `json:"phone" db:"phone" validate:"required"`
	Attempts    int    `json:"-" db:"attempts"`
	Status      string `json:"-" db:"status"`
	SendAt      int64  `json:"-" db:"send_at"`
	ConfirmedAt int64  `json:"-" db:"confirmed_at"`
	CreatedAt   string `json:"-" db:"created_at"`
	UpdatedAt   string `json:"-" db:"updated_at"`
	OTP         string `json:"otp,omitempty" validate:"required"`
}

type OtpInstance struct {
	Attempts    *int    `json:"-" db:"attempts"`
	Status      *string `json:"-" db:"status"`
	SendAt      *int64  `json:"-" db:"send_at"`
	ConfirmedAt *int64  `json:"-" db:"confirmed_at"`
}
