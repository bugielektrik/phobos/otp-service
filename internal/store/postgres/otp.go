package postgres

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/otp-service/internal/model"
)

type Otp struct {
	db     *sqlx.DB
	logger hclog.Logger
}

func NewOtpStore(db *sqlx.DB, logger hclog.Logger) *Otp {
	return &Otp{
		db:     db,
		logger: logger,
	}
}

func (s *Otp) Create(data *model.Otp) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		INSERT INTO otps (key, secret, phone, attempts, status, send_at, confirmed_at) 
		VALUES(:key, :secret, :phone, :attempts, :status, :send_at, :confirmed_at)`,
		data)
	return err
}

func (s *Otp) GetByKey(key string) (*model.Otp, error) {
	query := `
		SELECT *
		FROM otps
		WHERE key=:key`
	args := map[string]interface{}{
		"key": key,
	}
	data, err := s.getRow("GetByKey", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Otp) DeleteExpiredTokens(tokenExpires string) {
	logger := s.logger.With("operation", "DeleteExpiredTokens")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		DELETE FROM otps
		WHERE id IN (
		    SELECT id
		    FROM otps
		    WHERE created_at < (CURRENT_TIMESTAMP - INTERVAL '`+tokenExpires+` seconds')
		)`,
		map[string]interface{}{})
	if err != nil {
		logger.Error("failed to get", "error", err)
	}
}

func (s *Otp) Update(ctx context.Context, tx *sqlx.Tx, key string, data *model.OtpInstance) error {
	setValues := make([]string, 0)

	if data.Attempts != nil {
		setValues = append(setValues, "attempts=:attempts")
	}

	if data.Status != nil {
		setValues = append(setValues, "status=:status")
	}

	if data.SendAt != nil {
		setValues = append(setValues, "send_at=:send_at")
	}

	if data.ConfirmedAt != nil {
		setValues = append(setValues, "confirmed_at=:confirmed_at")
	}

	setValues = append(setValues, "updated_at=CURRENT_TIMESTAMP")

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE otps SET %s WHERE key='%s'", setQuery, key)

	err := s.update(ctx, tx, "Update", query, data)
	return err
}

func (s *Otp) Lock(ctx context.Context, key string) (func(), *sqlx.Tx, error) {
	logger := s.logger.With("operation", "Lock", "key", key)

	query := `SELECT * FROM otps WHERE key=:key FOR UPDATE`
	args := map[string]interface{}{
		"key": key,
	}

	logger.Debug("begin transaction")
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		logger.Error("failed to begin transaction", "error", err)
		return nil, nil, err
	}

	rows, err := tx.NamedQuery(query, args)
	if err != nil {
		logger.Error("failed to execute statement", "error", err)
		return nil, nil, err
	}
	defer rows.Close()

	logger.Debug("locked")
	return func() {
		err := tx.Commit()
		if err != nil {
			logger.Error("error while unlocking", "error", err)
			return
		}

		logger.Debug("unlocked")
	}, tx, nil
}

func (s *Otp) getRow(operation, query string, args interface{}) (*model.Otp, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	row, err := s.db.NamedQueryContext(ctx, query, args)
	if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer row.Close()

	if !row.Next() {
		logger.Debug("not found")
		return nil, nil
	}

	data := new(model.Otp)
	err = row.StructScan(&data)
	if err != nil {
		logger.Error("failed to scan into struct", "error", err)
		return nil, err
	}

	return data, nil
}

func (s *Otp) update(ctx context.Context, tx *sqlx.Tx, operation, query string, args interface{}) error {
	logger := s.logger.With("operation", operation)

	logger.Debug("start exec")
	finalize := func() error { return nil }
	if tx == nil {
		ttx, err := s.db.BeginTxx(ctx, nil)
		if err != nil {
			// failed to start transaction
			logger.Error("failed to start transaction", "error", err)
			return err
		}
		finalize = func() error {
			err = ttx.Commit()
			if err != nil {
				logger.Error("failed to commit", "error", err)
				return err
			}

			logger.Debug("committed update")
			return nil
		}
		tx = ttx
	}

	_, err := tx.NamedExec(query, args)
	if err != nil {
		logger.Error("failed to update", "error", err)
		return err
	}

	logger.Info("updated")
	return finalize()
}
