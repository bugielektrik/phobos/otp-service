package postgres

import (
	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	Otp *Otp
}

func New(db *sqlx.DB, logger hclog.Logger) *Store {
	return &Store{
		Otp: NewOtpStore(db, logger),
	}
}
