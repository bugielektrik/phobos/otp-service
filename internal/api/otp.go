package api

import (
	"context"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/otp-service/internal/model"
)

type OtpService interface {
	Create(phone, debug string) (*model.Otp, error)
	GetByID(ctx context.Context, otp *model.Otp) (*model.Otp, error)
	DeleteExpiredTokens()
}

type Otp struct {
	service OtpService
}

func NewOtpHandler(s OtpService) *Otp {
	return &Otp{service: s}
}

func (h *Otp) getOtp(c *fiber.Ctx) error {
	// Bind params
	phone := c.Query("phone")
	debug := c.Query("debug")

	// Write data to DB
	otpSrc, err := h.service.Create(phone, debug)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(otpSrc)
}

func (h *Otp) checkOtp(c *fiber.Ctx) error {
	// Bind params
	otpDest := new(model.Otp)
	if err := c.BodyParser(otpDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Validate params
	if err := validator.New().Struct(otpDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Read data from database
	ctx := context.WithValue(c.Context(), "key", otpDest.Key)
	otpSrc, err := h.service.GetByID(ctx, otpDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if otpSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (h *Otp) deleteExpiredTokens() {
	h.service.DeleteExpiredTokens()
}
