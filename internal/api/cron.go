package api

import (
	"github.com/robfig/cron/v3"
)

func (h *Handler) InitCron() *cron.Cron {
	// Init cron handler
	crontab := cron.New()

	// Init processor
	_, _ = crontab.AddFunc("@every 10s", func() { h.Otp.deleteExpiredTokens() })

	return crontab
}
