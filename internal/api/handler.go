package api

type Dependencies struct {
	OtpService OtpService
}

type Handler struct {
	Otp *Otp
}

func New(d Dependencies) *Handler {
	return &Handler{
		Otp: NewOtpHandler(d.OtpService),
	}
}
