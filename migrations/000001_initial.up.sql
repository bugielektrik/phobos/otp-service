CREATE TABLE IF NOT EXISTS otps (
    id              SERIAL PRIMARY KEY,
    created_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    key             VARCHAR NOT NULL UNIQUE,
    secret          VARCHAR NOT NULL,
    phone           VARCHAR NOT NULL,
    send_at         INTEGER NOT NULL,
    confirmed_at    INTEGER NOT NULL,
    attempts        INT NOT NULL DEFAULT 0,
    status          VARCHAR NOT NULL DEFAULT '1'
);